package playground.fs;

/*-
 * #%L
 * A File Scanner App
 * %%
 * Copyright (C) 2017 - 2020 OSGL (Open Source General Library)
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.junit.Test;
import org.osgl.util.IO;
import osgl.ut.TestBase;

import java.io.File;
import java.util.List;

public abstract class LogFileScannerTestBase extends TestBase {

    private List<String> expected;

    public LogFileScannerTestBase() {
        expected = IO.readLines(LogFileScannerTestBase.class.getResource("/test.result"));
    }

    protected abstract LogFileScanner getScanner();

    @Test
    public void test() throws Exception {
        List<String> result = getScanner().doScan(new File("src/test/resources"), "Login");
        eq(expected, result);
    }
}
