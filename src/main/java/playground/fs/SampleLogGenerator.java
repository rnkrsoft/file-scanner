package playground.fs;

/*-
 * #%L
 * A File Scanner App
 * %%
 * Copyright (C) 2017 - 2020 OSGL (Open Source General Library)
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.osgl.$;
import org.osgl.exception.UnexpectedException;
import org.osgl.util.IO;
import org.osgl.util.S;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class SampleLogGenerator {

    public static final String LOG_DIR = "logs";

    enum WeekDay {
        Mon, Tue, Wed, Thu, Fri
    }

    enum Operation {
        Login, Logout, View, Create, Delete, Edit, Patch
    }

    public static class LogItem {
        public WeekDay weekDay;
        public Operation operation;
        public String firstName;
        public String lastName;

        @Override
        public String toString() {
            return S.concat(weekDay, ", ", operation, ", ", firstName, " ", lastName);
        }
    }

    private final List<String> fnList;
    private final List<String> lnList;

    public SampleLogGenerator() {
        URL url = SampleLogGenerator.class.getResource("/first_name.list");
        fnList = IO.read(url).toLines();
        url = SampleLogGenerator.class.getResource("/last_name.list");
        lnList = IO.read(url).toLines();

        File logDir = new File("logs");
        if (!logDir.exists() && !logDir.mkdir()) {
            throw new UnexpectedException("Cannot make logs dir");
        }

        final int LINES_PER_FILE = 2 * 1000 * 1000;
        final int FILES = 100;
        for (int i = 0; i < FILES; ++i) {
            String fileName = "log" + i + ".log";
            List<String> lines = new ArrayList<>();
            for (int j = 0; j < LINES_PER_FILE; ++j) {
                String line = generateLogItem().toString();
                lines.add(line);
            }
            File file = new File(LOG_DIR, fileName);
            IO.write((S.join("\n", lines)), file);
        }
    }

    private LogItem generateLogItem() {
        LogItem item = new LogItem();
        item.weekDay = $.random(WeekDay.class);
        item.operation = $.random(Operation.class);
        item.firstName = $.random(fnList);
        item.lastName = $.random(lnList);
        return item;
    }

    public static void main(String[] args) {
        new SampleLogGenerator().generateLogItem();
    }
}
