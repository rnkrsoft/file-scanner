package playground.fs;

/*-
 * #%L
 * A File Scanner App
 * %%
 * Copyright (C) 2017 - 2020 OSGL (Open Source General Library)
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.File;
import java.util.List;

/**
 * A `LogFileScanner` can be used to simulate the following shell command:
 *
 * ```
 * cat *.log | grep 'Login' | sort | uniq -c | sort -nr
 * ```
 */
public interface LogFileScanner {

    /**
     * Scan all log file (*.log) inside the log file dir, capture all lines contains
     * `keyword` specified.
     *
     * Then it shall
     * * sort the filtered lines
     * * count the duplicate lines
     * * attach the duplicate number to each corresponding line
     * * sort the result in descending order
     *
     * And finally it shall return the result in a list.
     *
     * @param logFileDir the log file dir, assume no sub dir exists.
     * @param keyword the keyword to filter out the lines
     * @return a list of result as specified above
     */
    List<String> doScan(File logFileDir, String keyword) throws Exception;

}
