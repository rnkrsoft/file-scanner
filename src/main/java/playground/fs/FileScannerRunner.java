package playground.fs;

/*-
 * #%L
 * A File Scanner App
 * %%
 * Copyright (C) 2017 - 2020 OSGL (Open Source General Library)
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import playground.fs.rnkrsoft.CatLogFileScanner;
import org.osgl.$;
import org.osgl.util.C;
import org.osgl.util.S;
import playground.fs.green.GreenLogFileScanner;
import playground.fs.rnkrsoft.CatLogFileScanner2;

import java.io.File;
import java.util.List;

/**
 * Test and benchmark file scanners
 */
public class FileScannerRunner {

    private final File logDir;

    FileScannerRunner() {
        logDir = new File(SampleLogGenerator.LOG_DIR);
    }

    private void run(LogFileScanner scanner) throws Exception {
        long begin = $.ms();
        System.out.printf("\n\nRunning %s ...\n", scanner.getClass().getSimpleName());
        List<String> result = scanner.doScan(logDir, "Login");
        long end = $.ms();
        System.out.println("# of items: " + result.size());
        System.out.println(S.join("\n", C.list(result).head(13)));
        System.out.printf("It takes %sms to run %s\n", end - begin, scanner.getClass().getSimpleName());
    }


    public static void main(String[] args) throws Exception {
        FileScannerRunner runner = new FileScannerRunner();
        runner.run(new GreenLogFileScanner());
        runner.run(new CatLogFileScanner2());
    }

}
