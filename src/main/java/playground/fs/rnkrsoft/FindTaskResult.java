package playground.fs.rnkrsoft;

/*-
 * #%L
 * A File Scanner App
 * %%
 * Copyright (C) 2017 - 2020 OSGL (Open Source General Library)
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.util.HashMap;
import java.util.Map;

/**
 * Created by test on 2020/3/5.
 * 存储查找到的结果
 */
public class FindTaskResult {
    /**
     * 键为字符串， 值为出现次数
     */
    final Map<String, Integer> result = new HashMap<String, Integer>();

    public Map<String, Integer> getResult() {
        return result;
    }

    public FindTaskResult() {
    }
    /**
     * 增加字符串，不存在线程安全问题
     *
     * @param content 内容
     */
    public void add(String content) {
        Integer count = result.get(content);
        if (count == null) {
            count = 1;
        } else {
            count = count + 1;
        }
        result.put(content, count);
    }
}
