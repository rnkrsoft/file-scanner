package playground.fs.rnkrsoft;

/*-
 * #%L
 * A File Scanner App
 * %%
 * Copyright (C) 2017 - 2020 OSGL (Open Source General Library)
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.*;
import java.util.Map;
import java.util.concurrent.Callable;

/**
 * Created by test on 2020/3/5.
 * 查找任务
 */
public class FindTask implements Callable<FindTaskResult> {
    /**
     * 读取的任务文件
     */
    File file;
    /**
     * 搜索字符串
     */
    String searchStr;
    final Map<String, Integer> map;
    /**
     * 字符串查找器
     */
    final StringFinder stringFinder = new StringFinderImpl();

    public FindTask(File file, String searchStr) {
        this.file = file;
        this.searchStr = searchStr;
        this.map = null;
    }

    public FindTask(File file, String searchStr, Map<String, Integer> map) {
        this.file = file;
        this.searchStr = searchStr;
        this.map = map;
    }
    /**
     * 任务体
     * @return 任务结果
     * @throws IOException
     */
    FindTaskResult run0() throws IOException {
        FileReader reader = new FileReader(file);
        if (map == null){
            return stringFinder.find(new BufferedReader(reader), searchStr);
        }else{
            stringFinder.find(new BufferedReader(reader), searchStr, map);
            return null;
        }

    }

    @Override
    public FindTaskResult call() throws Exception {
        try {
            return run0();
        } catch (IOException e) {
            //nothing
            e.printStackTrace();
            return null;
        }
    }
}
