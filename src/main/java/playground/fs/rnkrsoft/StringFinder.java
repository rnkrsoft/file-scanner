package playground.fs.rnkrsoft;

/*-
 * #%L
 * A File Scanner App
 * %%
 * Copyright (C) 2017 - 2020 OSGL (Open Source General Library)
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Map;

/**
 * Created by test on 2020/3/5.
 * 字符串查找
 */
public interface StringFinder {
    /**
     * 查找
     * @param reader 字符流
     * @param searchStr 搜索指定的字符串
     * @return 任务对象
     */
    FindTaskResult find(BufferedReader reader, String searchStr) throws IOException;

    /**
     * 查找
     * @param reader 字符流
     * @param searchStr 搜索指定的字符串
     * @return 任务对象
     */
    void find(BufferedReader reader, String searchStr, Map<String, Integer> map) throws IOException;
}
