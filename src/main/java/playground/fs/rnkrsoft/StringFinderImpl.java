package playground.fs.rnkrsoft;

/*-
 * #%L
 * A File Scanner App
 * %%
 * Copyright (C) 2017 - 2020 OSGL (Open Source General Library)
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Map;

/**
 * Created by test on 2020/3/5.
 * 字符串查找器
 */
public class StringFinderImpl implements StringFinder{
    @Override
    public FindTaskResult find(BufferedReader reader, String searchStr) throws IOException {
        FindTaskResult taskResult = new FindTaskResult();
        try {
            String line = null;
            //逐行读取内容并判断是否未要查找字符串开始
            while ((line = reader.readLine()) != null){
                if (line.contains(searchStr)){
                    taskResult.add(line);
                }
            }
        }finally {
            if (reader != null){
                reader.close();
            }
        }
        return taskResult;
    }

    @Override
    public void find(BufferedReader reader, String searchStr, Map<String, Integer> map) throws IOException {
        try {
            String line = null;
            //逐行读取内容并判断是否未要查找字符串开始
            while ((line = reader.readLine()) != null){
                if (line.contains(searchStr)){
                    Integer count = map.get(line);
                    if (count == null) {
                        count = 1;
                    } else {
                        count = count + 1;
                    }
                    map.put(line, count);
                }
            }
        }finally {
            if (reader != null){
                reader.close();
            }
        }
    }
}
